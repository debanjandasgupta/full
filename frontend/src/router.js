import Vue from 'vue'
import Router from 'vue-router'
import home_page from './components/home.vue'
import user_login_page from './components/login.vue'
import user_registration_page from './components/register.vue'
import user_home_page from './components/userHome.vue'
import user_view_cart_page from './components/userViewCart.vue'
import seller_registration_page from './components/sellerRegister.vue'
import seller_login_page from './components/sellerLogin.vue'
import seller_home_page from './components/sellerHome.vue'
import seller_update_product_page from './components/sellerUpdateProduct.vue'
import seller_delete_product_page from './components/sellerDeleteProduct.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            component: home_page
        },
        {
            path: '/login',
            component: user_login_page
        },
        {
            path: '/register',
            component: user_registration_page
        },
        {
            path: '/home',
            component: user_home_page
        },
        {
            path: '/view_cart',
            component: user_view_cart_page
        },
        {
            path: '/seller/register',
            component: seller_registration_page
        },
        {
            path: '/seller/login',
            component: seller_login_page
        },
        {
            path: '/seller/home',
            component: seller_home_page
        },
        {
            path: '/seller/update_product',
            component: seller_update_product_page
        },
        {
            path: '/seller/delete_product',
            component: seller_delete_product_page
        }
    ]
})