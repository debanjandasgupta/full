const expr = require('express');
let router = expr.Router();
const controller = require('../controller/index')

//User operations
router.route("/user/login")
    .post(controller.loginUsers.loginMaker)
router.route("/user/register")
    .post(controller.registerUsers.registrationMaker)
router.route("/user/registration/otpVerification")
    .post(controller.otpVerification.otpVerifier)
router.route("/user/product/add_to_cart")
    .post(controller.user.authToken, controller.user.add_to_cart)
router.route("/user/product/view_cart")
    .get(controller.user.authToken, controller.user.view_cart)
router.route("/user/product/remove_from_cart")
    .post(controller.user.authToken, controller.user.delete_from_cart)

//Seller operations
router.route("/seller/login")
    .post(controller.loginSeller.loginMaker)
router.route("/seller/register")
    .post(controller.registerSeller.registrationMaker)
router.route("/seller/registration/otpVerification")
    .post(controller.otpVerification.otpVerifier)
router.route("/seller/product/add_new_product")
    .post(controller.product.authToken, controller.product.add_product)
router.route("/seller/product/update_product")
    .post(controller.product.authToken, controller.product.update_product)
router.route("/seller/product/get_products")
    .get(controller.product.authToken, controller.product.get_all_products_for_seller)
router.route("/seller/product/delete_product")
    .post(controller.product.authToken, controller.product.delete_product)


//Universal operations
router.route("/products").get(controller.product.get_all_products)

module.exports = router;