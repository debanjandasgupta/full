'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('products', {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true,
      notNull: true
    },
    seller_id: {
      type: 'int',
      notNull: true
    },
    name: {
      type: 'string',
      notNull: true
    },
    description: {
      type: 'string',
      notNull: true
    },
    price: {
      type: 'real',
      notNull: true
    },
    quantity: {
      type: 'int',
      notNull: true
    },
    createdAt: {
      type: 'datetime',
      notNull: false
    },
    updatedAt: {
      type: 'datetime',
      notNull: false
    },
    deletedAt: {
      type: 'datetime',
      notNull: false
    }
  });
};

exports.down = function(db) {
  return db.dropTable('products');
};

exports._meta = {
  "version": 1
};
