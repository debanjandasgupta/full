const Joi = require('joi');

const addProductSchema = Joi.object({
    seller_id: Joi.number().integer().required(),
    name: Joi.string().required(),
    description: Joi.string().required(),
    price: Joi.number().required(),
    quantity: Joi.number().integer().required()
});


module.exports = addProductSchema;