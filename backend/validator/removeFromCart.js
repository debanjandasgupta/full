const Joi = require('joi');

const removeCartSchema = Joi.object({
    id: Joi.number().integer().required()
});


module.exports = removeCartSchema;