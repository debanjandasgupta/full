module.exports = {
    login_validator: require('./login_validator'),
    registration_validator: require('./registration_validator'),
    otp_validator: require('./otp_validator'),
    add_product_validator: require('./add_product_validator'),
    update_product_validator: require('./update_product_validator'),
    delete_product_validator: require('./deleteProductValidator'),
    add_to_cart_validator: require('./addCartValidator'),
    remove_from_cart_validator: require('./removeFromCart')
}