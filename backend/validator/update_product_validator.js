const Joi = require('joi');

const productUpdateSchema = Joi.object({
    id: Joi.number().integer().required(),
    name: Joi.string(),
    description: Joi.string(),
    price: Joi.number(),
    quantity: Joi.number().integer()
});


module.exports = productUpdateSchema;