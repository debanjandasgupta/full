const Joi = require('joi');

const otpSchema = Joi.object({
    email: Joi.string().email().lowercase().required(),
    otp: Joi.string().required()
});


module.exports = otpSchema;