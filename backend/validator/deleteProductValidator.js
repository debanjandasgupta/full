const Joi = require('joi');

const productDeleteSchema = Joi.object({
    id: Joi.number().integer().required()
});


module.exports = productDeleteSchema;