module.exports = {
    user: require('./usersModel'),
    otp: require('./otpModel'),
    product: require('./productModel'),
    cart: require('./cartModel')
}