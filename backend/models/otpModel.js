const Sequelize = require('sequelize');
const sequelize = require('../db/db');


const Otps = sequelize.define("otps", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    user_email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    otp: {
        type: Sequelize.STRING,
        allowNull: false
    },
    createdAt: {
        type: Sequelize.DATE
    },
    updatedAt: {
        type: Sequelize.DATE
    },
    deletedAt: {
        type: Sequelize.DATE
    }
});



module.exports = Otps;