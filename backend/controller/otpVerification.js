require('dotenv').config()
const models = require('../models/index')
const validator = require('../validator/index')



exports.otpVerifier = async (req, res) => {
    try{
        const result = await validator.otp_validator.validateAsync({
            email: req.body.email,
            otp: req.body.otp
        })
        let count = await models.otp.count({
            where: {
                user_email: result.email,
                otp: result.otp
            }
        })
        if (count == 1){
            await models.otp.destroy({
                where: {
                    user_email: result.email,
                    otp: result.otp
                }
            })
            await models.user.update({
                status: 'VERIFIED'
            },
            {
                where: {
                    email: result.email
                }
            })
            res.send({
                status: 200,
                message: 'Account verified.'
            })
        }
    }
    catch (err) {
        res.send({
            status: 422,
            message: err.message
        });
    }
}