module.exports = {
    loginUsers: require('./loginUser'),
    registerUsers: require('./registerUser'),
    otpVerification: require('./otpVerification'),
    loginSeller: require('./loginSeller'),
    registerSeller: require('./registerSeller'),
    product: require('./product_operations'),
    user: require('./userOperations')
}