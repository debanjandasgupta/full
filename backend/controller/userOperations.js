require('dotenv').config()
const jwt = require('jsonwebtoken')
const Sequelize = require('sequelize');
const models = require('../models/index')
const validator = require('../validator/index')


//Token authentication
exports.authToken = (req, res, next) => {
    const tokenHeader = req.headers['authorization'];
    const token = tokenHeader && tokenHeader.split(" ")[1];
    if (token === null) {
        return res.send({
            status_code: 401,
            message: 'No token provided'
        });
    }

    jwt.verify(token, process.env.PRIVATE_KEY, (err, user) => {
        if (err) {
            console.log(err);
            return res.send({
                status_code: 403,
                message: "Token is no longer valid."
            });
        }
        req.user = user;
        next();
    })
}

//Get cart details
exports.view_cart = async (req, res) => {
    if (req.user.user_role === "USER") {
        const cart_data = await models.cart.findAll({
            where: {
                user_id: req.user.user_id
            },
            raw: true
        })
        let m_data = cart_data.map(async (cart) => {
            const product = await models.product.findOne({
                where: {
                    id: cart.product_id
                },
                raw: true
            })
            return product
        })
        let cart_m = await Promise.all(m_data)
        let arrdata = []
        let totalPrice = 0
        cart_m.map(c => {
            let f = 0
            if (arrdata.length > 0) {
                arrdata.map(d => {
                    if (d.product_id == c.id){
                        d.quantity = d.quantity + 1
                        totalPrice = totalPrice + c.price
                        f = 1
                        return
                    }
                })
                if (f == 0){
                    totalPrice = totalPrice + c.price
                    arrdata.push({
                        product_id: c.id,
                        product_name: c.name,
                        product_price: c.price,
                        quantity: 1
                    })
                }
            }
            else {
                totalPrice = totalPrice + c.price
                arrdata.push({
                    product_id: c.id,
                    product_name: c.name,
                    product_price: c.price,
                    quantity: 1
                })
            }
        })
        if (arrdata.length > 0){
            res.send({
                status: 200,
                message: {
                    data: arrdata,
                    total_price: totalPrice 
                }
            })
        }
        else{
            res.send({
                status: 200,
                message: "Add some products in your cart"
            })
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}


//Add to cart
exports.add_to_cart = async (req, res) => {
    if (req.user.user_role === "USER") {
        try {
            const results = await validator.add_to_cart_validator.validateAsync({
                user_id: req.user.user_id,
                product_id: req.body.product_id
            });
            models.cart.create({
                user_id: results.user_id,
                product_id: results.product_id
            }).then(() => {
                res.send({
                    status: 200,
                    message: `Product added to your cart.`
                });
            })
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}



//Remove from cart
exports.delete_from_cart = async (req, res) => {
    if (req.user.user_role === "USER") {
        try {
            const results = await validator.remove_from_cart_validator.validateAsync({
                id: req.body.product_id
            });
            models.cart.destroy({
                where: {
                    user_id: req.user.user_id,
                    product_id: results.id
                }
            }).then(() => {
                res.send({
                    status: 200,
                    message: `Product removed from your cart.`
                });
            })
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}

