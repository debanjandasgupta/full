require('dotenv').config()
const models = require('../models/index')
const validator = require('../validator/index')
const nodemailer = require("nodemailer");
const schedule = require('node-schedule')


exports.registrationMaker = async (req, res) => {
    try {
        const result = await validator.registration_validator.validateAsync({
            fullname: req.body.fullname,
            email: req.body.email,
            password: req.body.password,
            role: req.body.role,
        })
        let otp = await getOtp()
        let available = await models.user.count({
            where: {
                email: result.email,
                role: result.role
            }
        })
        if (available > 0) {
            return res.send({
                status: 201,
                message: 'Email already registered.'
            })
        }
        else {
            await models.user.create({
                fullname: result.fullname,
                email: result.email,
                password: result.password,
                role: result.role,
                status: 'PENDING'
            })
            await models.otp.create({
                user_email: result.email,
                otp: otp
            })

            let date = new Date();
            date.setMinutes(date.getMinutes()+15);


            schedule.scheduleJob(date, async function(){
                let count = await models.user.count({
                    where: {
                        email: result.email,
                        status: 'PENDING'
                    }
                })
                if (count == 1){
                    await models.user.destroy({
                        where: {
                            email: result.email
                        }
                    })
                    await models.otp.destroy({
                        where: {
                            user_email: result.email
                        }
                    })
                }
            });


            await sendOtp(result.email, otp)
            res.send({
                status: 200,
                message: 'OTP Sent.'
            })
        }
    } catch (error) {
        res.send({
            status: 422,
            message: error.message
        });
    }
}

async function getOtp() {
    let r = require('crypto').randomBytes(2).toString('hex')
    let count = await models.otp.count({
        where: {
            otp: r
        }
    })
    if (count == 0) {
        return r
    }
    else {
        getOtp()
    }
}


async function sendOtp(email, otp) {
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: process.env.email,
            pass: process.env.password
        },
    });
    var message = {
        to: email,
        subject: "Verify Otp",
        text: "Your otp is below",
        html: `<h1>${otp}</h1>`
    };
    await transporter.sendMail(message)
}