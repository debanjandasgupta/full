require('dotenv').config()
const jwt = require('jsonwebtoken')
const models = require('../models/index')
const validator = require('../validator/index')


//Token authentication
exports.authToken = (req, res, next) => {
    const tokenHeader = req.headers['authorization'];
    const token = tokenHeader && tokenHeader.split(" ")[1];
    if (token === null) {
        return res.send({
            status_code: 401,
            message: 'No token provided'
        });
    }

    jwt.verify(token, process.env.PRIVATE_KEY, (err, user) => {
        if (err) {
            console.log(err);
            return res.send({
                status_code: 403,
                message: "Token is no longer valid."
            });
        }
        req.user = user;
        next();
    })
}


//Add new product
exports.add_product = async (req, res) => {
    if (req.user.user_role === "SELLER" || req.user.user_role === "ADMIN") {
        try {
            const results = await validator.add_product_validator.validateAsync({
                seller_id: req.user.user_id,
                name: req.body.name,
                description: req.body.desc,
                price: req.body.price,
                quantity: req.body.quantity
            });
            models.product.create({
                seller_id: results.seller_id,
                name: results.name,
                description: results.description,
                price: results.price,
                quantity: results.quantity
            }).then(() => {
                let s = {
                    status: 200,
                    message: `Product added.`
                };
                res.send(s);
            })
        }
        catch (err) {
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}

//Get all products
exports.get_all_products = async (req, res) => {
    let data = await models.product.findAll()
    let products_data = await data.map(products => {
        return {
            product_id: products.id,
            product_name: products.name,
            product_description: products.description,
            product_price: products.price,
            product_quantity: products.quantity
        }
    })
    res.send(products_data)
}


//Get all product by a seller
exports.get_all_products_for_seller = async (req, res) => {
    if (req.user.user_role === "SELLER" || req.user.user_role === "ADMIN") {
        try{
            let products = await models.product.findAll({
                where: {
                    seller_id: req.user.user_id
                }
            })
            let data = await products.map(product => {
                return {
                    product_id: product.id,
                    product_name: product.name,
                    product_description: product.description,
                    product_price: product.price,
                    product_quantity: product.quantity
                }
            })
            res.send(data)
        }
        catch(err){
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}



//Update product
exports.update_product = async (req, res) => {
    if (req.user.user_role === "SELLER" || req.user.user_role === "ADMIN") {
        try{
            const results = await validator.update_product_validator.validateAsync({
                id: req.body.id,
                name: req.body.name,
                description: req.body.desc,
                price: req.body.price,
                quantity: req.body.quantity
            });
            models.product.update({
                name: results.name,
                description: results.description,
                price: results.price,
                quantity: results.quantity
            },
            {
                where: {
                    id: results.id
                }
            }
            ).then(() => {
                let s = {
                    status: 200,
                    message: `Product updated.`
                };
                res.send(s);
            })
        }
        catch(err){
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}



//Delete product
exports.delete_product = async (req, res) => {
    if (req.user.user_role === "SELLER" || req.user.user_role === "ADMIN") {
        try{
            const results = await validator.delete_product_validator.validateAsync({
                id: req.body.product_id
            })  
            models.product.destroy({
                where: {
                    id : results.id
                }
            }).then(() => {
                res.send({
                    status: 200,
                    message: 'Product Deleted.'
                })
            })        
        }
        catch(err){
            res.send({
                status_code: 422,
                message: err.message
            });
        }
    }
    else {
        res.send({
            status_code: 213,
            message: "Authenctication error."
        });
    }
}