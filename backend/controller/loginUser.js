require('dotenv').config()
const jwt = require('jsonwebtoken')
const models = require('../models/index')
const validator = require('../validator/index')



exports.loginMaker = async (req, res) => {
    try{
        const result = await validator.login_validator.validateAsync({
            email: req.body.email,
            password: req.body.password
        })
        let count = await models.user.count({
            where: {
                email: result.email,
                password: result.password,
                role: "USER",
                status: "VERIFIED"
            }
        });
        if (count == 1){
            let user = await models.user.findOne({
                where: {
                    email: result.email,
                    password: result.password,
                    role: "USER",
                    status: "VERIFIED"
                }
            })
            let user_details = {
                user_id: user.id,
                user_fullname: user.fullname,
                user_role: user.role
            }
            let accessToken = jwt.sign(user_details, process.env.PRIVATE_KEY);
            res.json({
                status: 200,
                message: 'Bearer '+ accessToken
            })
        }
        else{
            res.send({
                status: 201,
                message: "wrong credentials"
            })
        }
    }
    catch (err) {
        res.send({
            status: 422,
            message: err.message
        });
    }
}