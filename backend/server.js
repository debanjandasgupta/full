// Importing modules
const express = require('express')
const routes = require('./route/routes')
const app = express()
var cors = require('cors')

let jsonParser = require('body-parser').json()
app.use(jsonParser)
app.use(cors())


// Routes declaration
app.use('/api/operations', routes)



// Starting app
const port = process.env.PORT || 4040
app.listen(port, () => {console.log(`listening to ${port}`)})